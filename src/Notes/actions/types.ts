import { INote } from '../../interfaces/note.interface';

export const LOAD_NOTES = 'LOAD_NOTES';
export const LOAD_NOTES_SUCCESS = 'LOAD_NOTES_SUCCESS';

interface LoadNotesAction {
    type: typeof LOAD_NOTES;
  }
  
  export interface LoadNotesSuccess {
    type: typeof LOAD_NOTES_SUCCESS;
    payload: INote[];
  }

  export type NotesActionTypes = LoadNotesAction | LoadNotesSuccess;