import { INote } from '../../interfaces/note.interface';
import { NotesActionTypes } from '../actions/types';

export interface NotesState {
    list: INote[];
}

const initialState: NotesState = {
    list: [],
  };

export const notesReducer = (state: NotesState = initialState, action: NotesActionTypes): NotesState => {
    switch (action.type) {
        default:
            return state;
    }
};
