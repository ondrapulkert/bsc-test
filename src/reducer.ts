import { connectRouter } from 'connected-react-router';
import { History } from 'history';
import { combineReducers } from 'redux';
import { notesReducer } from './Notes/reducer';

export interface IRootState {}

const rootReducer = (history: History) =>
    combineReducers({
        router: connectRouter(history),
        notes: notesReducer,
    });

export default rootReducer;
