import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { Action, applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk, { ThunkAction } from 'redux-thunk';

import rootReducer, { IRootState } from './reducer';

export const history = createBrowserHistory();
export type AppThunk = ThunkAction<void, IRootState, null, Action<string>>;

export default function configureStore(preloadedState?: any) {
    const store = createStore(
        rootReducer(history), // root reducer with router state
        preloadedState,
        composeWithDevTools(applyMiddleware(routerMiddleware(history), thunk))
    );

    return store;
}
